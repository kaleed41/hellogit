#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{

	string str1 = "hello";
	string str2 = str1 + " world";

	cout << "str1 = " << str1 << endl;
	cout << "str2 = " << str2 << endl;
	cout << "the 4th character is" << str1[3] << endl;

	if(str1 == "hello")
		cout << "hi there" << endl;
	
	if(str2 != "world")
		cout << "out of this world" << endl;

	cout << "-----------" << endl;
		
	vector<char> v;

	vector<int> v3 = {17,5,6};

	vector<int> v1;
	
	v1.push_back(3);
	v1.push_back(7);
	v1.push_back(6);
	v1.push_back(2);
	v1.push_back(10);


	for(int i = 0; i < 4; i++)
	{
		cout << v1[i] << endl;
	}

     cout << "----------" << endl;

	for(int i =0; i < 3; i++)
	{
		cout << v3[i] << endl;
	}

	return 0;
}